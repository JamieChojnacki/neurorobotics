# `NeuroRobotics`: Reinforcement Learning Infrastructure for Physics-Based Simulation.

Software stack for physics-based simulation and Reinforcement
Learning environments, using Bullet physics and stable baselines3
